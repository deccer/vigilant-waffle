# Ultima Fabricator
[![License: GPL3](https://img.shields.io/cran/l/asd?color=%23CC2233&label=License)](https://github.com/deccer/vigilant-waffle/blob/master/license)
![CodeFactor](https://www.codefactor.io/repository/github/deccer/vigilant-waffle/badge)
![Build-Debug](https://github.com/deccer/vigilant-waffle/workflows/Build-Debug/badge.svg)
![Build-Release](https://github.com/deccer/vigilant-waffle/workflows/Build-Release/badge.svg)

Ultima Online build tools to edit and modify your shard.
